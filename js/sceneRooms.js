/*
 * CS32310 Assignment - Kieran Dunbar - kid10
 * scene.js - Functions to draw room-specific objects
 */

var drawLeftRoomObjects = function() {
    //Loop through each drawer material and set each as double sided (not configurable in JSON)
    $.each(objectGeometries.drawers.material.materials, function(key, material) {
        material.side = THREE.DoubleSide;
    });
    var drawers = {"object": objectGeometries.drawers, "position": new THREE.Vector3(-97.25, 5.5, -80), "scale": new THREE.Vector3(18, 18, 18), "rotation": new THREE.Vector3(0, 90, 0)};
    drawObject(drawers);

    var bed = {"geometry": objectGeometries.bed, "position": new THREE.Vector3(-72.5, 0, -22), "scale": new THREE.Vector3(10, 10, 10), "texture": textures.bed, "rotation": new THREE.Vector3(0, 90, 0), "material": "Lambert"};
    drawObject(bed);

    var wardrobe = {"geometry": objectGeometries.wardrobe, "position": new THREE.Vector3(-39, 0, -45), "texture": textures.wardrobe, "rotation": new THREE.Vector3(0, 180, 0), "material": "Phong"};
    drawObject(wardrobe);
}

var drawCenterRoomObjects = function() {
    var coatStand = {"geometry": objectGeometries.coatstand, "position": new THREE.Vector3(12.5, 0, -34), "scale": new THREE.Vector3(0.8, 0.8, 0.8), "texture": textures.metal, "material": "Phong"};
    drawObject(coatStand);

    var fireplace = {
        "geometry": objectGeometries.fireplace,
        "position": new THREE.Vector3(0, 7.6, -95.4),
        "material": "Lambert",
        "texture": textures.brickWall,
        "textureRepeat": new THREE.Vector2(6, 6),
        "rotation": new THREE.Vector3(0, 270, 0),
        "scale": new THREE.Vector3(4.5, 4.5, 4.5),
        "doubleSided": true
    };
    drawObject(fireplace);

    var fireplaceGrill = {
        "geometry": objectGeometries.fireplaceGrill,
        "position": new THREE.Vector3(0, 7.6, -95.4),
        "material": "Phong",
        "texture": textures.metal,
        "rotation": new THREE.Vector3(0, 270, 0),
        "scale": new THREE.Vector3(4.5, 4.5, 4.5),
        "doubleSided": true
    };
    drawObject(fireplaceGrill);

    var fireplaceWoodGeometry = new THREE.CylinderGeometry(1, 1, 10, 10);
    var fireplaceWood = [
        {"geometry": fireplaceWoodGeometry, "position": new THREE.Vector3(0, 1, -96), "texture": textures.burntWood, "rotation": new THREE.Vector3(90, 0, 70), "Material": "Lambert"},
        {"geometry": fireplaceWoodGeometry, "position": new THREE.Vector3(0, 3, -95), "texture": textures.burntWood, "rotation": new THREE.Vector3(0, 0, 80), "Material": "Lambert"},
        {"geometry": fireplaceWoodGeometry, "position": new THREE.Vector3(0, 3, -97), "texture": textures.burntWood, "rotation": new THREE.Vector3(40, 0, 120), "Material": "Lambert"}
    ];
    drawObjects(fireplaceWood);
}

var drawRightRoomObjects = function() {
    var fridge = {"object": objectGeometries.fridge, "position": new THREE.Vector3(61, 0, -97), "scale": new THREE.Vector3(1.3, 1.3, 1.3)};
    drawObject(fridge);

    //Loop through each counter material and set each as double sided (not configurable in JSON)
    $.each(objectGeometries.counter.material.materials, function(key, material) {
        material.side = THREE.DoubleSide;
    });
    var counterMap = [
        {"object": objectGeometries.counterNoDoors, "position": new THREE.Vector3(42, 0.1, -96), "scale": new THREE.Vector3(0.535, 0.8, 0.8)},
        {"object": objectGeometries.counter, "position": new THREE.Vector3(47, 0.1, -96), "scale": new THREE.Vector3(0.8, 0.8, 0.8)},
        {"object": objectGeometries.counter, "position": new THREE.Vector3(53, 0.1, -96), "scale": new THREE.Vector3(0.8, 0.8, 0.8)},
        {"object": objectGeometries.counter, "position": new THREE.Vector3(69, 0.1, -96), "scale": new THREE.Vector3(0.8, 0.8, 0.8)},
        {"object": objectGeometries.counter, "position": new THREE.Vector3(81.4, 0.1, -96), "scale": new THREE.Vector3(0.8, 0.8, 0.8)},
        {"object": objectGeometries.counter, "position": new THREE.Vector3(87.4, 0.1, -96), "scale": new THREE.Vector3(0.8, 0.8, 0.8)},
        {"object": objectGeometries.counter, "position": new THREE.Vector3(93.4, 0.1, -96), "scale": new THREE.Vector3(0.8, 0.8, 0.8)},
        {"object": objectGeometries.counterNoDoors, "position": new THREE.Vector3(98.2, 0.1, -96), "scale": new THREE.Vector3(0.485, 0.8, 0.8)},
    ]
    drawObjects(counterMap);

    var oven = {"object": objectGeometries.oven, "position": new THREE.Vector3(75.1, 0, -96), "scale": new THREE.Vector3(0.6, 0.55, 0.6)};
    drawObject(oven);

    var rug = {"position": new THREE.Vector3(70, 0, -60), "size": new THREE.Vector3(30, 0.2, 20), "texture": textures.woolFloor, "material": "Lambert", "segments": new THREE.Vector2(3, 1)};
    drawObject(rug);

    var chair = {
        "geometry": objectGeometries.chair,
        "position": new THREE.Vector3(0, 0, 0),
        "material": "Phong",
        "texture": textures.chair,
        "scale": new THREE.Vector3(6, 6, 6)
    };
    var chairObj = drawObject(chair, true);
    var chairMap = [
        {"object": chairObj, "position": new THREE.Vector3(66, 0, -29.5)},
        {"object": chairObj, "position": new THREE.Vector3(74, 0, -29.5)},
        {"object": chairObj, "position": new THREE.Vector3(66, 0, -14.5), "rotation": new THREE.Vector3(0, 180, 0)},
        {"object": chairObj, "position": new THREE.Vector3(74, 0, -14.5), "rotation": new THREE.Vector3(0, 180, 0)},
        {"object": chairObj, "position": new THREE.Vector3(82, 0, -22), "rotation": new THREE.Vector3(0, 270, 0)},
        {"object": chairObj, "position": new THREE.Vector3(58, 0, -22), "rotation": new THREE.Vector3(0, 90, 0)}
    ];
    drawObjects(chairMap);
}
