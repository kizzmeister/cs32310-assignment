/*
 * CS32310 Assignment - Kieran Dunbar - kid10
 * controls.js - Handles the mouse and keyboard controls
 */

//Handle Mouse controls, using the Pointer Lock API
//With help from http://www.smartjava.org/content/html5-use-pointer-lock-api-restrict-mouse-movement-element
//and https://www.html5rocks.com/en/tutorials/pointerlock/intro/
document.addEventListener('pointerlockchange', pointerLockChange, false);
document.addEventListener('mozpointerlockchange', pointerLockChange, false);
document.addEventListener('webkitpointerlockchange', pointerLockChange, false);

//When the canvas is clicked, attempt to activate the pointer lock
$("canvas").click(function() {
    if (mouseControlsEnabled === false) return;

    var canvas = $(this).get()[0];
    canvas.requestPointerLock = canvas.requestPointerLock ||
                                canvas.mozRequestPointerLock ||
                                canvas.webkitRequestPointerLock;
    canvas.requestPointerLock();
});

//When the pointer lock is changed, toggle the PointerLockControls library accordingly
function pointerLockChange() {
    var canvas = $("canvas").get()[0];
    if (document.pointerLockElement === canvas ||
        document.mozPointerLockElement === canvas ||
        document.webkitPointerLockElement === canvas) {

        controls.enabled = true;
    } else {
        controls.enabled = false;
    }
}

//Handle Keyboard controls
var keyNames = ["up", "left", "down", "right", "shift", "pgUp", "pgDown", "r", "t"];
var keys = {
    16: "shift",
    33: "pgUp",
    34: "pgDown",
    37: "left",
    38: "up",
    39: "right",
    40: "down",
    65: "left",
    68: "right",
    82: "r",
    83: "down",
    84: "t",
    87: "up"
};
var keyMap = {};

for (var i = 0; i < keyNames.length; i++) {
    keyMap[keyNames[i]] = false;
}

$(document).keydown(function(event) {
    var keyCode = (event) ? event.keyCode : window.event.keyCode;

    if (keyCode in keys) {
        keyMap[keys[keyCode]] = true;
    }
});

$(document).keyup(function(event) {
    var keyCode = (event) ? event.keyCode : window.event.keyCode;

    if (keyCode in keys) {
        keyMap[keys[keyCode]] = false;
    }
});
