/*
 * CS32310 Assignment - Kieran Dunbar - kid10
 * loop.js - Renders and animates the scene
 */

var looping = true;
var doorOpen = false;

//Define and start the animation loop
var render = function() {
    //Ensure all resources have been loaded before initialising the world
    if (!loaded) {
        var loadStr = "Loading...<br /><br />";
        loadStr += numLoadedTextures + "/" + Object.keys(images).length + " Textures<br />";
        loadStr += numLoadedObjects + "/" + Object.keys(objectFiles).length + " Objects";
        $("#loading").html(loadStr);

        if (resourcesLoaded()) {
            loaded = true;
            $("#loading").hide();
            initWorld();
        }
    }

    //Move controls/camera depending on pressed keys
    //Note: Cannot just increment/decrement position/co-ordinates of camera, we need to translate along the axis. This ensures movement is relative to camera orientation.
    //With help from: https://github.com/mrdoob/three.js/blob/1ad1a0c2af602a33cf25245ae9651832b2c59a60/examples/misc_controls_pointerlock.html
    var walkSpeed = keyMap["shift"] ? 2.5 : 1.5;
    var camObj = controls.getObject();

    if (keyboardControlsEnabled) {
        //Walk controls
        if (keyMap["up"]) camObj.translateZ(-walkSpeed);
        if (keyMap["left"]) camObj.translateX(-walkSpeed);
        if (keyMap["down"]) camObj.translateZ(walkSpeed);
        if (keyMap["right"]) camObj.translateX(walkSpeed);

        //Reset camera
        if (keyMap['r'] && mouseControlsEnabled) controls.reset();
    }

    //Open and close door depending on camera proximity
    if (cameraWithinBounds(20, -20, 0, -60)) {
        doorOpen = true;
    } else {
        doorOpen = false;
    }
    animateDoor();

    //Handle actions in debug mode
    if (debug) {
        showDebugInfo();

        if (keyboardControlsEnabled) {
            if (keyMap['pgUp']) camObj.translateY(walkSpeed);
            if (keyMap['pgDown']) camObj.translateY(-walkSpeed);
            if (keyMap['t']) {
                toggleTopDown();
                keyMap['t'] = false;
            }
        }
    } else {
        //Restrict movement to within the world boundary
        camObj.position.x = Math.max(-boundary, Math.min(boundary, camObj.position.x));
        camObj.position.z = Math.max(-boundary, Math.min(boundary, camObj.position.z));
    }

    stats.update();
    renderer.render(scene, camera);
    if (looping) requestAnimationFrame(render);
};

var cameraWithinBounds = function(maxX, minX, maxZ, minZ) {
    var camPos = controls.getObject().position;
    if (camPos.x >= minX && camPos.x <= maxX && camPos.z >= minZ && camPos.z <= maxZ) return true;
}

var animateDoor = function() {
    if (!meshes.woodenDoor) return;
    var doorSpeed = 2;
    var door = meshes.woodenDoor;

    //If the door should be closed, rotate until rotY = 0, otherwise rotate until rotY = 90
    if (!doorOpen && door.rotation.y > degToRad(0)) {
        door.rotation.y -= degToRad(doorSpeed);
    } else if (doorOpen && door.rotation.y < degToRad(90)) {
        door.rotation.y += degToRad(doorSpeed);
    }
}

var degToRad = function(deg) {
    return deg * (Math.PI/180);
}

render();
