/*
 * CS32310 Assignment - Kieran Dunbar - kid10
 * sceneStructure.js - Functions to draw the house/scene structure
 */

 var drawSky = function() {
     var skyMap = [
         {"position": new THREE.Vector3(-1250, 685, 0), "size": new THREE.Vector2(2500, 1372), "texture": textures.skymap_negx, "rotation": new THREE.Vector3(0, 90, 0)},
         {"position": new THREE.Vector3(0, 685, 1250), "size": new THREE.Vector2(2500, 1372), "texture": textures.skymap_negz, "rotation": new THREE.Vector3(0, 180, 0)},
         {"position": new THREE.Vector3(1250, 685, 0), "size": new THREE.Vector2(2500, 1372), "texture": textures.skymap_posx, "rotation": new THREE.Vector3(0, -90, 0)},
         {"position": new THREE.Vector3(0, 1370, 0), "size": new THREE.Vector2(2500, 2500), "texture": textures.skymap_posy, "rotation": new THREE.Vector3(90, 0, 0)},
         {"position": new THREE.Vector3(0, 685, -1250), "size": new THREE.Vector2(2500, 1372), "texture": textures.skymap_posz}
     ];

     drawObjects(skyMap);
 }

var drawFloors = function() {
    var floorMap = [
        //Grass floor
        {"position": new THREE.Vector3(0, 0, -127), "size": new THREE.Vector2(310, 54), "texture": textures.grassFloor, "textureRepeat": new THREE.Vector2(23, 4)},
        {"position": new THREE.Vector3(-128, 0, 27), "size": new THREE.Vector2(54, 254), "texture": textures.grassFloor, "textureRepeat": new THREE.Vector2(4, 19)},
        {"position": new THREE.Vector3(-54, 0, 77), "size": new THREE.Vector2(94, 154), "texture": textures.grassFloor, "textureRepeat": new THREE.Vector2(7, 11)},
        {"position": new THREE.Vector3(-23, 0, -15), "size": new THREE.Vector2(32, 30), "texture": textures.grassFloor},
        {"position": new THREE.Vector3(23, 0, -15), "size": new THREE.Vector2(32, 30), "texture": textures.grassFloor},
        {"position": new THREE.Vector3(54, 0, 77), "size": new THREE.Vector2(94, 154), "texture": textures.grassFloor, "textureRepeat": new THREE.Vector2(7, 11)},
        {"position": new THREE.Vector3(128, 0, 27), "size": new THREE.Vector2(54, 254), "texture": textures.grassFloor, "textureRepeat": new THREE.Vector2(4, 19)},

        //House floors
        {"position": new THREE.Vector3(-70, 0, -50), "size": new THREE.Vector2(60, 100), "texture": textures.carpetFloor, "material": "Lambert", "segments": new THREE.Vector2(6, 10)},
        {"position": new THREE.Vector3(0, 0, -65), "size": new THREE.Vector2(80, 72), "texture": textures.woodenFloor, "material": "Phong"},
        {"position": new THREE.Vector3(70, 0, -50), "size": new THREE.Vector2(60, 100), "texture": textures.woodenFloor, "textureRepeat": new THREE.Vector2(3, 5), "material": "Phong"},

        //Stone floor (outside path)
        {"position": new THREE.Vector3(0, 0, 63), "size": new THREE.Vector2(14, 184), "texture": textures.stoneFloor},
    ];

    drawObjects(floorMap, false, false, new THREE.Vector3(270, 0, 0));
}

var drawWalls = function() {
    var wallMap = [
        /* Left room inner wall */
        //B Wall
        {"position": new THREE.Vector3(-70, 20.5, -100), "size": new THREE.Vector2(60, 9), "texture": textures.creamWall},
        {"position": new THREE.Vector3(-53.5, 12, -100), "size": new THREE.Vector2(27, 8), "texture": textures.creamWall},
        {"position": new THREE.Vector3(-86.5, 12, -100), "size": new THREE.Vector2(27, 8), "texture": textures.creamWall},
        {"position": new THREE.Vector3(-70, 4, -100), "size": new THREE.Vector2(60, 8), "texture": textures.creamWall},
        //R Door
        {"position": new THREE.Vector3(-40, 12.5, -85), "size": new THREE.Vector2(30, 25), "texture": textures.creamWall, "rotation": new THREE.Vector3(0, 270, 0), "doubleSided": true},
        {"position": new THREE.Vector3(-40, 21.5, -65), "size": new THREE.Vector2(10, 7), "texture": textures.creamWall, "textureRepeat": new THREE.Vector2(1, 1), "rotation": new THREE.Vector3(0, 270, 0), "doubleSided": true},
        {"position": new THREE.Vector3(-40, 12.5, -30), "size": new THREE.Vector2(60, 25), "texture": textures.creamWall, "rotation": new THREE.Vector3(0, 270, 0), "doubleSided": true},
        //F Wall
        {"position": new THREE.Vector3(-70, 20.5, 0), "size": new THREE.Vector2(60, 9), "texture": textures.creamWall, "rotation": new THREE.Vector3(0, 180, 0)},
	    {"position": new THREE.Vector3(-52, 12, 0), "size": new THREE.Vector2(24, 8), "texture": textures.creamWall, "rotation": new THREE.Vector3(0, 180, 0)},
	    {"position": new THREE.Vector3(-88, 12, 0), "size": new THREE.Vector2(24, 8), "texture": textures.creamWall, "rotation": new THREE.Vector3(0, 180, 0)},
	    {"position": new THREE.Vector3(-70, 4, 0), "size": new THREE.Vector2(60, 8), "texture": textures.creamWall, "rotation": new THREE.Vector3(0, 180, 0)},
        //L Wall
        {"position": new THREE.Vector3(-100, 20.5, -50), "size": new THREE.Vector2(100, 9), "texture": textures.creamWall, "rotation": new THREE.Vector3(0, 90, 0)},
        {"position": new THREE.Vector3(-100, 12, -22), "size": new THREE.Vector2(44, 8), "texture": textures.creamWall, "rotation": new THREE.Vector3(0, 90, 0)},
        {"position": new THREE.Vector3(-100, 12, -78), "size": new THREE.Vector2(44, 8), "texture": textures.creamWall, "rotation": new THREE.Vector3(0, 90, 0)},
        {"position": new THREE.Vector3(-100, 4, -50), "size": new THREE.Vector2(100, 8), "texture": textures.creamWall, "rotation": new THREE.Vector3(0, 90, 0)},

        /* Center room inner wall */
        //B Wall + Windows
        {"position": new THREE.Vector3(0, 20.5, -100), "size": new THREE.Vector2(80, 9), "texture": textures.creamWall},
        {"position": new THREE.Vector3(-33, 12, -100), "size": new THREE.Vector2(14, 8), "texture": textures.creamWall},
        {"position": new THREE.Vector3(0, 12, -100), "size": new THREE.Vector2(40, 8), "texture": textures.creamWall},
        {"position": new THREE.Vector3(33, 12, -100), "size": new THREE.Vector2(14, 8), "texture": textures.creamWall},
        {"position": new THREE.Vector3(0, 4, -100), "size": new THREE.Vector2(80, 8), "texture": textures.creamWall},
        //L Door
        {"position": new THREE.Vector3(-39, 12.5, -85), "size": new THREE.Vector2(30, 25), "texture": textures.creamWall, "rotation": new THREE.Vector3(0, 90, 0), "doubleSided": true},
        {"position": new THREE.Vector3(-39, 21.5, -65), "size": new THREE.Vector2(10, 7), "texture": textures.creamWall, "textureRepeat": new THREE.Vector2(1, 1), "rotation": new THREE.Vector3(0, 90, 0), "doubleSided": true},
        {"position": new THREE.Vector3(-39, 12.5, -45), "size": new THREE.Vector2(30, 25), "texture": textures.creamWall, "rotation": new THREE.Vector3(0, 90, 0), "doubleSided": true},
        //F Door + Windows
        {"position": new THREE.Vector3(-22, 20.5, -30), "size": new THREE.Vector2(34, 9), "texture": textures.creamWall, "rotation": new THREE.Vector3(0, 180, 0)},
        {"position": new THREE.Vector3(-32, 12, -30), "size": new THREE.Vector2(14, 8), "texture": textures.creamWall, "rotation": new THREE.Vector3(0, 180, 0)},
        {"position": new THREE.Vector3(-12, 12, -30), "size": new THREE.Vector2(14, 8), "texture": textures.creamWall, "rotation": new THREE.Vector3(0, 180, 0)},
        {"position": new THREE.Vector3(-22, 4, -30), "size": new THREE.Vector2(34, 8), "texture": textures.creamWall, "rotation": new THREE.Vector3(0, 180, 0)},

        {"position": new THREE.Vector3(0, 21.5, -30), "size": new THREE.Vector2(12, 7), "texture": textures.creamWall, "textureRepeat": new THREE.Vector2(1, 1), "rotation": new THREE.Vector3(0, 180, 0)},

        {"position": new THREE.Vector3(22, 20.5, -30), "size": new THREE.Vector2(34, 9), "texture": textures.creamWall, "rotation": new THREE.Vector3(0, 180, 0)},
        {"position": new THREE.Vector3(32, 12, -30), "size": new THREE.Vector2(14, 8), "texture": textures.creamWall, "rotation": new THREE.Vector3(0, 180, 0)},
        {"position": new THREE.Vector3(12, 12, -30), "size": new THREE.Vector2(14, 8), "texture": textures.creamWall, "rotation": new THREE.Vector3(0, 180, 0)},
        {"position": new THREE.Vector3(22, 4, -30), "size": new THREE.Vector2(34, 8), "texture": textures.creamWall, "rotation": new THREE.Vector3(0, 180, 0)},
        //R Door
        {"position": new THREE.Vector3(39, 12.5, -85), "size": new THREE.Vector2(30, 25), "texture": textures.creamWall, "rotation": new THREE.Vector3(0, 270, 0), "doubleSided": true},
        {"position": new THREE.Vector3(39, 21.5, -65), "size": new THREE.Vector2(10, 7), "texture": textures.creamWall, "textureRepeat": new THREE.Vector2(1, 1), "rotation": new THREE.Vector3(0, 270, 0), "doubleSided": true},
        {"position": new THREE.Vector3(39, 12.5, -45), "size": new THREE.Vector2(30, 25), "texture": textures.creamWall, "rotation": new THREE.Vector3(0, 270, 0), "doubleSided": true},

        /* Right room inner wall */
        //B Wall
        {"position": new THREE.Vector3(70, 20.5, -100), "size": new THREE.Vector2(60, 9), "texture": textures.creamWall},
        {"position": new THREE.Vector3(53.5, 12, -100), "size": new THREE.Vector2(27, 8), "texture": textures.creamWall},
        {"position": new THREE.Vector3(86.5, 12, -100), "size": new THREE.Vector2(27, 8), "texture": textures.creamWall},
        {"position": new THREE.Vector3(70, 4, -100), "size": new THREE.Vector2(60, 8), "texture": textures.creamWall},
        //L Door
        {"position": new THREE.Vector3(40, 12.5, -85), "size": new THREE.Vector2(30, 25), "texture": textures.creamWall, "rotation": new THREE.Vector3(0, 90, 0), "doubleSided": true},
        {"position": new THREE.Vector3(40, 21.5, -65), "size": new THREE.Vector2(10, 7), "texture": textures.creamWall, "textureRepeat": new THREE.Vector2(1, 1), "rotation": new THREE.Vector3(0, 90, 0), "doubleSided": true},
        {"position": new THREE.Vector3(40, 12.5, -30), "size": new THREE.Vector2(60, 25), "texture": textures.creamWall, "rotation": new THREE.Vector3(0, 90, 0), "doubleSided": true},
        //F Wall
        {"position": new THREE.Vector3(70, 20.5, 0), "size": new THREE.Vector2(60, 9), "texture": textures.creamWall, "rotation": new THREE.Vector3(0, 180, 0)},
	    {"position": new THREE.Vector3(52, 12, 0), "size": new THREE.Vector2(24, 8), "texture": textures.creamWall, "rotation": new THREE.Vector3(0, 180, 0)},
	    {"position": new THREE.Vector3(88, 12, 0), "size": new THREE.Vector2(24, 8), "texture": textures.creamWall, "rotation": new THREE.Vector3(0, 180, 0)},
	    {"position": new THREE.Vector3(70, 4, 0), "size": new THREE.Vector2(60, 8), "texture": textures.creamWall, "rotation": new THREE.Vector3(0, 180, 0)},
        //R Wall
        {"position": new THREE.Vector3(100, 20.5, -50), "size": new THREE.Vector2(100, 9), "texture": textures.creamWall, "rotation": new THREE.Vector3(0, 270, 0)},
        {"position": new THREE.Vector3(100, 12, -22), "size": new THREE.Vector2(44, 8), "texture": textures.creamWall, "rotation": new THREE.Vector3(0, 270, 0)},
        {"position": new THREE.Vector3(100, 12, -78), "size": new THREE.Vector2(44, 8), "texture": textures.creamWall, "rotation": new THREE.Vector3(0, 270, 0)},
        {"position": new THREE.Vector3(100, 4, -50), "size": new THREE.Vector2(100, 8), "texture": textures.creamWall, "rotation": new THREE.Vector3(0, 270, 0)},

        /* Outer House walls */
        //B C Wall
        {"position": new THREE.Vector3(0, 20.5, -101), "size": new THREE.Vector2(80, 9), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(5.13, 0.5625), "rotation": new THREE.Vector3(0, 180, 0)},
        {"position": new THREE.Vector3(-33, 12, -101), "size": new THREE.Vector2(14, 8), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(1, 0.5), "rotation": new THREE.Vector3(0, 180, 0)},
        {"position": new THREE.Vector3(0, 12, -101), "size": new THREE.Vector2(40, 8), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(2.94, 0.5), "rotation": new THREE.Vector3(0, 180, 0)},
        {"position": new THREE.Vector3(33, 12, -101), "size": new THREE.Vector2(14, 8), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(1, 0.5), "rotation": new THREE.Vector3(0, 180, 0)},
        {"position": new THREE.Vector3(0, 4, -101), "size": new THREE.Vector2(80, 8), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(5.13, 0.5), "rotation": new THREE.Vector3(0, 180, 0)},
        //B L Wall
        {"position": new THREE.Vector3(-70.5, 20.5, -101), "size": new THREE.Vector2(61, 9), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(4.49, 0.5625), "rotation": new THREE.Vector3(0, 180, 0)},
        {"position": new THREE.Vector3(-53.25, 12, -101), "size": new THREE.Vector2(27.5, 8), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(2.02, 0.5), "rotation": new THREE.Vector3(0, 180, 0)},
        {"position": new THREE.Vector3(-87.25, 12, -101), "size": new THREE.Vector2(27.5, 8), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(2.02, 0.5), "rotation": new THREE.Vector3(0, 180, 0)},
        {"position": new THREE.Vector3(-70.5, 4, -101), "size": new THREE.Vector2(61, 8), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(4.49, 0.5), "rotation": new THREE.Vector3(0, 180, 0)},
        //B R Wall
        {"position": new THREE.Vector3(70.5, 20.5, -101), "size": new THREE.Vector2(61, 9), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(4.49, 0.5625), "rotation": new THREE.Vector3(0, 180, 0)},
        {"position": new THREE.Vector3(53.25, 12, -101), "size": new THREE.Vector2(27.5, 8), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(2.02, 0.5), "rotation": new THREE.Vector3(0, 180, 0)},
        {"position": new THREE.Vector3(87.25, 12, -101), "size": new THREE.Vector2(27.5, 8), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(2.02, 0.5), "rotation": new THREE.Vector3(0, 180, 0)},
        {"position": new THREE.Vector3(70.5, 4, -101), "size": new THREE.Vector2(61, 8), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(4.49, 0.5), "rotation": new THREE.Vector3(0, 180, 0)},

        //L Wall
        {"position": new THREE.Vector3(-101, 20.5, -50), "size": new THREE.Vector2(102, 9), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(7.5, 0.5625), "rotation": new THREE.Vector3(0, 270, 0)},
        {"position": new THREE.Vector3(-101, 12, -21.5), "size": new THREE.Vector2(45, 8), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(3.31, 0.5), "rotation": new THREE.Vector3(0, 270, 0)},
        {"position": new THREE.Vector3(-101, 12, -78.5), "size": new THREE.Vector2(45, 8), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(3.31, 0.5), "rotation": new THREE.Vector3(0, 270, 0)},
        {"position": new THREE.Vector3(-101, 4, -50), "size": new THREE.Vector2(102, 8), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(7.5, 0.5), "rotation": new THREE.Vector3(0, 270, 0)},

        //R Wall
        {"position": new THREE.Vector3(101, 20.5, -50), "size": new THREE.Vector2(102, 9), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(7.5, 0.5625), "rotation": new THREE.Vector3(0, 90, 0)},
        {"position": new THREE.Vector3(101, 12, -21.5), "size": new THREE.Vector2(45, 8), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(3.31, 0.5), "rotation": new THREE.Vector3(0, 90, 0)},
        {"position": new THREE.Vector3(101, 12, -78.5), "size": new THREE.Vector2(45, 8), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(3.31, 0.5), "rotation": new THREE.Vector3(0, 90, 0)},
        {"position": new THREE.Vector3(101, 4, -50), "size": new THREE.Vector2(102, 8), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(7.5, 0.5), "rotation": new THREE.Vector3(0, 90, 0)},

        //L Front Wall
        {"position": new THREE.Vector3(-70, 20.5, 1), "size": new THREE.Vector2(62, 9), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(4.56, 0.5625)},
        {"position": new THREE.Vector3(-51.5, 12, 1), "size": new THREE.Vector2(25, 8), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(1.84, 0.5)},
        {"position": new THREE.Vector3(-88.5, 12, 1), "size": new THREE.Vector2(25, 8), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(1.84, 0.5)},
        {"position": new THREE.Vector3(-70, 4, 1), "size": new THREE.Vector2(62, 8), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(4.56, 0.5)},
        //L Inner Wall
        {"position": new THREE.Vector3(-39, 12.5, -14), "size": new THREE.Vector2(30, 25), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(2.21, 1.5625), "rotation": new THREE.Vector3(0, 90, 0)},
        //L Center Wall
        {"position": new THREE.Vector3(-22, 17, -29), "size": new THREE.Vector2(34, 2), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(2.5, 0.125)},
        {"position": new THREE.Vector3(-32, 12, -29), "size": new THREE.Vector2(14, 8), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(1, 0.5)},
        {"position": new THREE.Vector3(-12, 12, -29), "size": new THREE.Vector2(14, 8), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(1, 0.5)},
        {"position": new THREE.Vector3(-22, 4, -29), "size": new THREE.Vector2(34, 8), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(2.5, 0.5)},
        //C Wall (above door)
        {"position": new THREE.Vector3(0, 21.5, -29), "size": new THREE.Vector2(78, 7), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(5, 0.5)},
        //R Center Wall
        {"position": new THREE.Vector3(22, 17, -29), "size": new THREE.Vector2(34, 2), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(2.5, 0.125)},
        {"position": new THREE.Vector3(32, 12, -29), "size": new THREE.Vector2(14, 8), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(1, 0.5)},
        {"position": new THREE.Vector3(12, 12, -29), "size": new THREE.Vector2(14, 8), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(1, 0.5)},
        {"position": new THREE.Vector3(22, 4, -29), "size": new THREE.Vector2(34, 8), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(2.5, 0.5)},
        //R Inner Wall
        {"position": new THREE.Vector3(39, 12.5, -14), "size": new THREE.Vector2(30, 25), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(2.21, 1.5625), "rotation": new THREE.Vector3(0, 270, 0)},
        //R Front Wall
        {"position": new THREE.Vector3(70, 20.5, 1), "size": new THREE.Vector2(62, 9), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(4.56, 0.5625)},
        {"position": new THREE.Vector3(51.5, 12, 1), "size": new THREE.Vector2(25, 8), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(1.84, 0.5)},
        {"position": new THREE.Vector3(88.5, 12, 1), "size": new THREE.Vector2(25, 8), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(1.84, 0.5)},
        {"position": new THREE.Vector3(70, 4, 1), "size": new THREE.Vector2(62, 8), "texture": textures.brickWall2, "textureRepeat": new THREE.Vector2(4.56, 0.5)},

        //Garden Wall
        {"position": new THREE.Vector3(0, 5, -155), "size": new THREE.Vector3(310, 10, 2), "texture": textures.brickWall},
        {"position": new THREE.Vector3(-155, 5, 0), "size": new THREE.Vector3(310, 10, 2), "texture": textures.brickWall, "rotation": new THREE.Vector3(0, 90, 0)},
        {"position": new THREE.Vector3(0, 5, 155), "size": new THREE.Vector3(310, 10, 2), "texture": textures.brickWall, "rotation": new THREE.Vector3(0, 180, 0)},
        {"position": new THREE.Vector3(155, 5, 0), "size": new THREE.Vector3(310, 10, 2), "texture": textures.brickWall, "rotation": new THREE.Vector3(0, 270, 0)}
    ];

    drawObjects(wallMap);
}

var drawRoof = function() {
    //Define a gableGeometry (as there is no built-in triangleGeometry)
    var gableGeometry = new THREE.Geometry();
    gableGeometry.vertices.push(
        new THREE.Vector3(0, 9.35, 0),
        new THREE.Vector3(-10, 0, 0),
        new THREE.Vector3(10, 0, 0)
    );
    gableGeometry.faces.push(new THREE.Face3(0, 1, 2));
    gableGeometry.faceVertexUvs[0].push([
        new THREE.Vector2(0, 0),
        new THREE.Vector2(0.5, 1),
        new THREE.Vector2(1, 0)
    ]);

    var roofMap = [
        //House ceilings
        {"position": new THREE.Vector3(-70, 25, -50), "size": new THREE.Vector2(60, 100), "texture": textures.creamWall, "textureRepeat": new THREE.Vector2(6, 8), "rotation": new THREE.Vector3(90, 0, 0)},
        {"position": new THREE.Vector3(0, 25, -65), "size": new THREE.Vector2(80, 72), "texture": textures.creamWall, "textureRepeat": new THREE.Vector2(8, 5.76), "rotation": new THREE.Vector3(90, 0, 0)},
        {"position": new THREE.Vector3(70, 25, -50), "size": new THREE.Vector2(60, 100), "texture": textures.creamWall, "textureRepeat": new THREE.Vector2(6, 8), "rotation": new THREE.Vector3(90, 0, 0)},

        //L Roof
        {"position": new THREE.Vector3(-86, 39.1, -50), "size": new THREE.Vector2(105, 44), "texture": textures.slateRoof, "textureRepeat": new THREE.Vector2(5.58, 2.76), "rotation": new THREE.Vector3(90, 43, 270), "doubleSided": true},
        {"position": new THREE.Vector3(-54, 39.1, -13.5), "size": new THREE.Vector2(32, 44), "texture": textures.slateRoof, "textureRepeat": new THREE.Vector2(1.70, 2.76), "rotation": new THREE.Vector3(90, 317, 90), "doubleSided": true},
        {"position": new THREE.Vector3(-54.75, 39.8, -64.75), "size": new THREE.Vector2(70.5, 42), "texture": textures.slateRoof, "textureRepeat": new THREE.Vector2(3.74, 2.64), "rotation": new THREE.Vector3(90, 317, 90), "doubleSided": true},
        {"position": new THREE.Vector3(-54, 39.1, -101.25), "size": new THREE.Vector2(2.5, 44), "texture": textures.slateRoof, "textureRepeat": new THREE.Vector2(0.13, 2.76), "rotation": new THREE.Vector3(90, 317, 90), "doubleSided": true},

        //L Roof Gable
        {"position": new THREE.Vector3(-70, 25, 1), "geometry": gableGeometry, "texture": textures.creamWall, "scale": new THREE.Vector3(3.1, 3.1, 3.1)},
        {"position": new THREE.Vector3(-70, 25, -101), "geometry": gableGeometry, "texture": textures.creamWall, "scale": new THREE.Vector3(3.1, 3.1, 3.1), "rotation": new THREE.Vector3(0, 180, 0)},

        //C Roof
        {"position": new THREE.Vector3(0, 38, -46), "size": new THREE.Vector2(75.5, 47.75), "texture": textures.slateRoof, "rotation": new THREE.Vector3(307, 0, 0), "doubleSided": true},
        {"position": new THREE.Vector3(0, 38, -84), "size": new THREE.Vector2(75.5, 47.75), "texture": textures.slateRoof, "rotation": new THREE.Vector3(53, 0, 0), "doubleSided": true},

        //C Roof Joins
        {"position": new THREE.Vector3(-53, 38.7, -47), "size": new THREE.Vector2(32, 45.25), "texture": textures.slateRoof, "textureRepeat": new THREE.Vector2(1.70, 2.84), "rotation": new THREE.Vector3(307, 0, 0), "doubleSided": true},
        {"position": new THREE.Vector3(-53, 38.7, -83), "size": new THREE.Vector2(32, 45.25), "texture": textures.slateRoof, "textureRepeat": new THREE.Vector2(1.70, 2.84), "rotation": new THREE.Vector3(53, 0, 0), "doubleSided": true},

        {"position": new THREE.Vector3(53, 38.7, -47), "size": new THREE.Vector2(32, 45.25), "texture": textures.slateRoof, "textureRepeat": new THREE.Vector2(1.70, 2.84), "rotation": new THREE.Vector3(307, 0, 0), "doubleSided": true},
        {"position": new THREE.Vector3(53, 38.7, -83), "size": new THREE.Vector2(32, 45.25), "texture": textures.slateRoof, "textureRepeat": new THREE.Vector2(1.70, 2.84), "rotation": new THREE.Vector3(53, 0, 0), "doubleSided": true},

        //R Roof
        {"position": new THREE.Vector3(86, 39.1, -50), "size": new THREE.Vector2(105, 44), "texture": textures.slateRoof, "textureRepeat": new THREE.Vector2(5.58, 2.76), "rotation": new THREE.Vector3(90, 317, 90), "doubleSided": true},
        {"position": new THREE.Vector3(54, 39.1, -13.5), "size": new THREE.Vector2(32, 44), "texture": textures.slateRoof, "textureRepeat": new THREE.Vector2(1.70, 2.76), "rotation": new THREE.Vector3(90, 43, 270), "doubleSided": true},
        {"position": new THREE.Vector3(54.75, 39.8, -64.75), "size": new THREE.Vector2(70.5, 42), "texture": textures.slateRoof, "textureRepeat": new THREE.Vector2(3.74, 2.64), "rotation": new THREE.Vector3(90, 43, 270), "doubleSided": true},
        {"position": new THREE.Vector3(54, 39.1, -101.25), "size": new THREE.Vector2(2.5, 44), "texture": textures.slateRoof, "textureRepeat": new THREE.Vector2(0.13, 2.76), "rotation": new THREE.Vector3(90, 43, 270), "doubleSided": true},

        //R Roof Gable
        {"position": new THREE.Vector3(70, 25, 1), "geometry": gableGeometry, "texture": textures.creamWall, "scale": new THREE.Vector3(3.1, 3.1, 3.1)},
        {"position": new THREE.Vector3(70, 25, -101), "geometry": gableGeometry, "texture": textures.creamWall, "scale": new THREE.Vector3(3.1, 3.1, 3.1), "rotation": new THREE.Vector3(0, 180, 0)},
    ];

    drawObjects(roofMap);
}

var drawDoors = function() {
    var doorMap = [
        //F Door Frame + Door
        {"position": new THREE.Vector3(-5, 8.5, -29.5), "size": new THREE.Vector3(0.75, 17.5, 1.5), "texture": textures.woodenFloor, "textureRepeat": new THREE.Vector2(1, 1)},
        {"position": new THREE.Vector3(0, 17.65, -29.5), "size": new THREE.Vector3(10.75, 0.75, 1.5), "texture": textures.woodenFloor, "textureRepeat": new THREE.Vector2(1, 1)},
        {"position": new THREE.Vector3(5, 8.5, -29.5), "size": new THREE.Vector3(0.75, 17.5, 1.5), "texture": textures.woodenFloor, "textureRepeat": new THREE.Vector2(1, 1)},
        {"position": new THREE.Vector3(0, 0, -29.5), "size": new THREE.Vector3(9.25, 0.75, 1.5), "texture": textures.woodenFloor, "textureRepeat": new THREE.Vector2(1, 1)},
        {"reference": "woodenDoor", "position": new THREE.Vector3(-4.65, 8.75, -29.5), "size": new THREE.Vector3(9.25, 17, 0.5), "texture": textures.woodenDoor, "animated": true},

        //L Arch Frame
        {"position": new THREE.Vector3(-39.5, 8.5, -60), "size": new THREE.Vector3(0.75, 17.5, 1.5), "texture": textures.woodenFloor, "textureRepeat": new THREE.Vector2(1, 1), "rotation": new THREE.Vector3(0, 90, 0)},
        {"position": new THREE.Vector3(-39.5, 17.65, -65), "size": new THREE.Vector3(10.75, 0.75, 1.5), "texture": textures.woodenFloor, "textureRepeat": new THREE.Vector2(1, 1), "rotation": new THREE.Vector3(0, 90, 0)},
        {"position": new THREE.Vector3(-39.5, 8.5, -70), "size": new THREE.Vector3(0.75, 17.5, 1.5), "texture": textures.woodenFloor, "textureRepeat": new THREE.Vector2(1, 1), "rotation": new THREE.Vector3(0, 90, 0)},
        {"position": new THREE.Vector3(-39.5, 0, -65), "size": new THREE.Vector3(9.25, 0.75, 1.5), "texture": textures.woodenFloor, "textureRepeat": new THREE.Vector2(1, 1), "rotation": new THREE.Vector3(0, 90, 0)},

        //R Arch Frame
        {"position": new THREE.Vector3(39.5, 8.5, -60), "size": new THREE.Vector3(0.75, 17.5, 1.5), "texture": textures.woodenFloor, "textureRepeat": new THREE.Vector2(1, 1), "rotation": new THREE.Vector3(0, 90, 0)},
        {"position": new THREE.Vector3(39.5, 17.65, -65), "size": new THREE.Vector3(10.75, 0.75, 1.5), "texture": textures.woodenFloor, "textureRepeat": new THREE.Vector2(1, 1), "rotation": new THREE.Vector3(0, 90, 0)},
        {"position": new THREE.Vector3(39.5, 8.5, -70), "size": new THREE.Vector3(0.75, 17.5, 1.5), "texture": textures.woodenFloor, "textureRepeat": new THREE.Vector2(1, 1), "rotation": new THREE.Vector3(0, 90, 0)},
        {"position": new THREE.Vector3(39.5, 0, -65), "size": new THREE.Vector3(9.25, 0.75, 1.5), "texture": textures.woodenFloor, "textureRepeat": new THREE.Vector2(1, 1), "rotation": new THREE.Vector3(0, 90, 0)},
    ];

    drawObjects(doorMap);

    //Translate the geometry of the door to enable rotations around the hinge
    //With help from: https://github.com/mrdoob/three.js/issues/1364
    var translationMatrix = new THREE.Matrix4().makeTranslation(4.65, 0, 0);
    meshes.woodenDoor.geometry.applyMatrix(translationMatrix);

    //Correct Door to have door handle on correct side (when viewed from either side)
    meshes.woodenDoor.geometry.faceVertexUvs[0][8][2].x = -1;
    meshes.woodenDoor.geometry.faceVertexUvs[0][9][1].x = -1;
    meshes.woodenDoor.geometry.faceVertexUvs[0][9][2].x = -1;
}

var drawWindows = function() {
	var smallWindow = [
		{"position": new THREE.Vector3(-3, 0, 0), "size": new THREE.Vector3(0.75, 8.75, 1.5), "texture": textures.woodenFloor, "textureRepeat": new THREE.Vector2(1, 1)},
        {"position": new THREE.Vector3(0, 4, 0), "size": new THREE.Vector3(5.25, 0.75, 1.5), "texture": textures.woodenFloor, "textureRepeat": new THREE.Vector2(1, 1)},
        {"position": new THREE.Vector3(3, 0, 0), "size": new THREE.Vector3(0.75, 8.75, 1.5), "texture": textures.woodenFloor, "textureRepeat": new THREE.Vector2(1, 1)},
        {"position": new THREE.Vector3(0, -4, 0), "size": new THREE.Vector3(5.25, 0.75, 1.5), "texture": textures.woodenFloor, "textureRepeat": new THREE.Vector2(1, 1)},
		{"position": new THREE.Vector3(0, 0, 0), "size": new THREE.Vector3(0.75, 8.75, 1), "texture": textures.woodenFloor, "textureRepeat": new THREE.Vector2(1, 1)},
        {"position": new THREE.Vector3(0, 0, 0), "size": new THREE.Vector3(5.25, 0.75, 1.1), "texture": textures.woodenFloor, "textureRepeat": new THREE.Vector2(1, 1)},
	];
	var smallWindowObj = drawObjects(smallWindow, true, true);

    var largeWindow = [
        {"position": new THREE.Vector3(-6, 0, 0), "size": new THREE.Vector3(0.75, 8.75, 1.5), "texture": textures.woodenFloor, "textureRepeat": new THREE.Vector2(1, 1)},
        {"position": new THREE.Vector3(0, 4, 0), "size": new THREE.Vector3(11.25, 0.75, 1.5), "texture": textures.woodenFloor, "textureRepeat": new THREE.Vector2(1, 1)},
        {"position": new THREE.Vector3(6, 0, 0), "size": new THREE.Vector3(0.75, 8.75, 1.5), "texture": textures.woodenFloor, "textureRepeat": new THREE.Vector2(1, 1)},
        {"position": new THREE.Vector3(0, -4, 0), "size": new THREE.Vector3(11.25, 0.75, 1.5), "texture": textures.woodenFloor, "textureRepeat": new THREE.Vector2(1, 1)},
        {"position": new THREE.Vector3(0, 0, 0), "size": new THREE.Vector3(0.75, 8.75, 1), "texture": textures.woodenFloor, "textureRepeat": new THREE.Vector2(1, 1)},
        {"position": new THREE.Vector3(0, 0, 0), "size": new THREE.Vector3(11.25, 0.75, 1.1), "texture": textures.woodenFloor, "textureRepeat": new THREE.Vector2(1, 1)},
    ];
    var largeWindowObj = drawObjects(largeWindow, true, true);

	var windowMap = [
		{"object": smallWindowObj, "position": new THREE.Vector3(-22, 12.375, -29.5)}, //Center Left Front Window
		{"object": smallWindowObj, "position": new THREE.Vector3(22, 12.375, -29.5)}, //Center Right Front Window
		{"object": smallWindowObj, "position": new THREE.Vector3(-23, 12.375, -100.5)}, //Center Left Back Window
		{"object": smallWindowObj, "position": new THREE.Vector3(23, 12.375, -100.5)}, //Center Right Back Window
		{"object": smallWindowObj, "position": new THREE.Vector3(-70.25, 12.375, -100.5)}, //Left Back Window
		{"object": smallWindowObj, "position": new THREE.Vector3(70.25, 12.375, -100.5)}, //Right Back Window

        {"object": largeWindowObj, "position": new THREE.Vector3(-70, 12.375, 0.5)}, //Left Front Window
        {"object": largeWindowObj, "position": new THREE.Vector3(70, 12.375, 0.5)}, //Right Front Window
        {"object": largeWindowObj, "position": new THREE.Vector3(-100.5, 12.375, -50), "rotation": new THREE.Vector3(0, 90, 0)}, //Left Side Window
        {"object": largeWindowObj, "position": new THREE.Vector3(100.5, 12.375, -50), "rotation": new THREE.Vector3(0, 270, 0)}, //Right Side Window
	];
    drawObjects(windowMap);
}

var drawLights = function() {
    var lightMap = [
        //Left room
        {"reference": "lightl1", "object": new THREE.PointLight(0xffffff, 0.9, 50), "position": new THREE.Vector3(-50, 20, -85)},
        {"reference": "lightl2", "object": new THREE.PointLight(0xffffff, 0.9, 50), "position": new THREE.Vector3(-85, 20, -85)},
        {"reference": "lightl3", "object": new THREE.PointLight(0xffffff, 0.9, 50), "position": new THREE.Vector3(-60, 20, -55)},
        {"reference": "lightl4", "object": new THREE.PointLight(0xffffff, 0.9, 50), "position": new THREE.Vector3(-85, 20, -55)},
        {"reference": "lightl5", "object": new THREE.PointLight(0xffffff, 0.9, 50), "position": new THREE.Vector3(-50, 20, -25)},
        {"reference": "lightl6", "object": new THREE.PointLight(0xffffff, 0.9, 50), "position": new THREE.Vector3(-95, 20, -22)},
        {"reference": "lightl7", "object": new THREE.PointLight(0xffffff, 0.9, 50), "position": new THREE.Vector3(-100, 15, -5)},

        //Center room
        {"reference": "lightc1", "object": new THREE.PointLight(0xffffff, 1, 50), "position": new THREE.Vector3(20, 24, -85)},
        {"reference": "lightc2", "object": new THREE.PointLight(0xffffff, 1, 50), "position": new THREE.Vector3(-20, 24, -85)},
        {"reference": "lightc3", "object": new THREE.PointLight(0xffffff, 1, 100), "position": new THREE.Vector3(20, 24, -40)},
        {"reference": "lightc4", "object": new THREE.PointLight(0xffffff, 1, 100), "position": new THREE.Vector3(-20, 24, -40)},
        {"reference": "lightc5", "object": new THREE.PointLight(0xffffff, 0.5, 50), "position": new THREE.Vector3(20, -15, -95)},
        {"reference": "lightc6", "object": new THREE.PointLight(0xffffff, 1, 50), "position": new THREE.Vector3(0, 40, -110)},
        {"reference": "lightc7", "object": new THREE.PointLight(0xffffff, 1, 50), "position": new THREE.Vector3(-10, 40, -95)},

        //Right room
        {"reference": "lightr1", "object": new THREE.PointLight(0xffffff, 1.5, 50), "position": new THREE.Vector3(85, 15, -97)},
        {"reference": "lightr2", "object": new THREE.PointLight(0xffffff, 1.5, 50), "position": new THREE.Vector3(50, 18, -97)},
        {"reference": "lightr3", "object": new THREE.PointLight(0xffffff, 1, 50), "position": new THREE.Vector3(85, 20, -50)},
        {"reference": "lightr4", "object": new THREE.PointLight(0xffffff, 1, 50), "position": new THREE.Vector3(50, 20, -50)},
        {"reference": "lightr5", "object": new THREE.PointLight(0xffffff, 1, 50), "position": new THREE.Vector3(85, 20, -10)},
        {"reference": "lightr6", "object": new THREE.PointLight(0xffffff, 1, 50), "position": new THREE.Vector3(50, 20, -10)},
        {"reference": "lightr7", "object": new THREE.PointLight(0xffffff, 0.5, 50), "position": new THREE.Vector3(70, 15, -40)},
    ];
    drawObjects(lightMap);

    //If we're in debug mode, display a helper object for each light
    if (debug) {
        $.each(lightMap, function(key, light) {
            scene.add(new THREE.PointLightHelper(meshes[light.reference], 1));
        });
    }
}
