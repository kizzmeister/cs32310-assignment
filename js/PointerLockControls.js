/**
 * CS32310 Assignment - Kieran Dunbar - kid10
 * PointerLockControls.js - Modified version of three.js example library to provide 'Minecraft' style mouse interaction
 *
 * From the three.js examples/js folder, used for the misc_pointerlockcontrols.html example
 * Source: https://github.com/mrdoob/three.js/blob/1ad1a0c2af602a33cf25245ae9651832b2c59a60/examples/js/controls/PointerLockControls.js
 *
 * @author mrdoob / http://mrdoob.com/
 */

THREE.PointerLockControls = function (camera) {
	var scope = this;
	camera.rotation.set(0, 0, 0);

	var pitchObject = new THREE.Object3D();
	pitchObject.add(camera);

	var yawObject = new THREE.Object3D();
	yawObject.position.y = 10;
	yawObject.add(pitchObject);

	var PI_2 = Math.PI / 2;

	var onMouseMove = function(event) {
		if (scope.enabled === false || mouseControlsEnabled === false) return;

		var movementX = event.movementX || event.mozMovementX || event.webkitMovementX || 0;
		var movementY = event.movementY || event.mozMovementY || event.webkitMovementY || 0;

		yawObject.rotation.y -= movementX * 0.002;
		pitchObject.rotation.x -= movementY * 0.002;

		pitchObject.rotation.x = Math.max(-PI_2, Math.min(PI_2, pitchObject.rotation.x));
	};

	this.dispose = function() {
		document.removeEventListener('mousemove', onMouseMove, false);
	};

	document.addEventListener('mousemove', onMouseMove, false);

	this.enabled = false;

	this.getObject = function() {
		return yawObject;
	};

	this.reset = function() {
		yawObject.position.x = 0;
		yawObject.position.y = 10;
		yawObject.position.z = 100;

		pitchObject.rotation.x = 0;
		yawObject.rotation.y = 0;
	};
};
