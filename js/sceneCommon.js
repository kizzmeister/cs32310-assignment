/*
 * CS32310 Assignment - Kieran Dunbar - kid10
 * sceneCommon.js - Functions to draw objects common between rooms
 */

var drawPaintings = function() {
    var paintingFrame = [
        {"position": new THREE.Vector3(-5.25, 0, 0.1), "size": new THREE.Vector3(0.5, 6, 0.2), "texture": textures.woodenFloor, "textureRepeat": new THREE.Vector2(1, 1)},
        {"position": new THREE.Vector3(0, 3.25, 0.1), "size": new THREE.Vector3(11, 0.5, 0.2), "texture": textures.woodenFloor, "textureRepeat": new THREE.Vector2(1, 1)},
        {"position": new THREE.Vector3(5.25, 0, 0.1), "size": new THREE.Vector3(0.5, 6, 0.2), "texture": textures.woodenFloor, "textureRepeat": new THREE.Vector2(1, 1)},
        {"position": new THREE.Vector3(0, -3.25, 0.1), "size": new THREE.Vector3(11, 0.5, 0.2), "texture": textures.woodenFloor, "textureRepeat": new THREE.Vector2(1, 1)}
    ];
    var paintingFrameObj = drawObjects(paintingFrame, true, true);
    var paintingMap = [
        //Center room
        {"position": new THREE.Vector3(-38, 12.5, -45), "object": paintingFrameObj, "rotation": new THREE.Vector3(0, 90, 0)},
        {"position": new THREE.Vector3(-38, 12.5, -45), "size": new THREE.Vector2(10, 6), "texture": textures.painting1, "rotation": new THREE.Vector3(0, 90, 0)},

        {"position": new THREE.Vector3(38, 12.5, -45), "object": paintingFrameObj, "rotation": new THREE.Vector3(0, 270, 0)},
        {"position": new THREE.Vector3(38, 12.5, -45), "size": new THREE.Vector2(10, 6), "texture": textures.painting6, "rotation": new THREE.Vector3(0, 270, 0)},

        //Left room
        {"position": new THREE.Vector3(-41, 12.5, -85), "object": paintingFrameObj, "rotation": new THREE.Vector3(0, 270, 0)},
        {"position": new THREE.Vector3(-41, 12.5, -85), "size": new THREE.Vector2(10, 6), "texture": textures.painting2, "rotation": new THREE.Vector3(0, 270, 0)},

        {"position": new THREE.Vector3(-41, 12.5, -25), "object": paintingFrameObj, "rotation": new THREE.Vector3(0, 270, 0)},
        {"position": new THREE.Vector3(-41, 12.5, -25), "size": new THREE.Vector2(10, 6), "texture": textures.painting5, "rotation": new THREE.Vector3(0, 270, 0)},

        //Right room
        {"position": new THREE.Vector3(99, 12.5, -22), "object": paintingFrameObj, "rotation": new THREE.Vector3(0, 270, 0)},
        {"position": new THREE.Vector3(99, 12.5, -22), "size": new THREE.Vector2(10, 6), "texture": textures.painting3, "rotation": new THREE.Vector3(0, 270, 0)},

        {"position": new THREE.Vector3(41, 12.5, -22), "object": paintingFrameObj, "rotation": new THREE.Vector3(0, 90, 0)},
        {"position": new THREE.Vector3(41, 12.5, -22), "size": new THREE.Vector2(10, 6), "texture": textures.painting4, "rotation": new THREE.Vector3(0, 90, 0)},
    ];
    drawObjects(paintingMap);
}

var drawTables = function() {
    //Loop through each table material and set each as double sided (not configurable in JSON)
    $.each(objectGeometries.table.material.materials, function(key, material) {
        material.side = THREE.DoubleSide;
    });
    var tableMap = [
        //Dining table
        {"object": objectGeometries.table, "position": new THREE.Vector3(70, 0, -20), "scale": new THREE.Vector3(6, 1.5, 4)},

        //Center room coffee table
        {"object": objectGeometries.table, "position": new THREE.Vector3(0, 0, -80), "scale": new THREE.Vector3(1.8, 1, 1.5)},
    ];
    drawObjects(tableMap);
}

var drawArmchairs = function() {
    var armchairMap = [
        //Center room
        {"geometry": objectGeometries.armchair, "position": new THREE.Vector3(-12.5, 0, -75), "texture": textures.chair2, "scale": new THREE.Vector3(4, 4, 4), "rotation": new THREE.Vector3(0, 150, 0), "material": "Phong"},
        {"geometry": objectGeometries.armchair, "position": new THREE.Vector3(12.5, 0, -75), "texture": textures.chair2, "scale": new THREE.Vector3(4, 4, 4), "rotation": new THREE.Vector3(0, 210, 0), "material": "Phong"},

        //Left room
        {"geometry": objectGeometries.armchair, "position": new THREE.Vector3(-60, 0, -94), "texture": textures.chair2, "scale": new THREE.Vector3(4, 4, 4), "material": "Phong"},
    ];
    drawObjects(armchairMap);
}
