/*
 * CS32310 Assignment - Kieran Dunbar - kid10
 * main.js - Core code to initialize the world before it is rendered
 */

var width = window.innerWidth;
var height = window.innerHeight;
var boundary = 150;
var debug = false;
var mouseControlsEnabled = true;
var keyboardControlsEnabled = true;
var loaded = false;
var meshes = {};

//Setup the WebGLRenderer
var renderer = new THREE.WebGLRenderer({
    antialias: true
});
renderer.setSize(width, height);
$("body").append(renderer.domElement);

//Initialise the Scene and Camera
var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera(65, width/height, 0.1, 4000);
scene.add(camera);

//Initialise the PointerLockControls library to handle mouse controls
var controls = new THREE.PointerLockControls(camera);
scene.add(controls.getObject());

var stats = new Stats();
stats.domElement.style.position = 'absolute';
stats.domElement.style.top = '0px';
$("body").append(stats.domElement);

//Setup and draw the world including floors, walls and the skymap
var initWorld = function() {
    //World Structure
    drawSky();
    drawWalls();
    drawFloors();
    drawRoof();
    drawDoors();
    drawWindows();
    drawLights();

    //Common Objects
    drawPaintings();
    drawTables();
    drawArmchairs();

    //Room Specific objects
    drawLeftRoomObjects();
    drawCenterRoomObjects();
    drawRightRoomObjects();

    //Set the camera looking at the door down the path
    controls.getObject().position.z = 100;
}

//Helper function to draw objects from a predefined list specifying position, size, texture, repeating factor and rotation
var drawObjects = function(list, removeFromScene, returnMerged, defaultRotation) {
	var objs = [];

    $.each(list, function(key, object) {
        objs[key] = drawObject(object, removeFromScene, defaultRotation);
    });

	if (returnMerged) {
		var retObj = new THREE.Object3D();
		$.each(objs, function(key, obj) {
			retObj.add(obj);
		});
		return retObj;
	}
}

//Helper function to draw an object specifying position, size, texture, repeating factor and rotation
var drawObject = function(object, removeFromScene, defaultRotation) {
    var geometry, texture, mesh;

    //Use the default rotation for this list if not defined
    if (!(object.rotation instanceof THREE.Vector3)) {
        object.rotation = (defaultRotation) ? defaultRotation : new THREE.Vector3(0, 0, 0);
    }

    if (object.object && (object.object instanceof THREE.Object3D || object.object instanceof THREE.Mesh)) {
        mesh = object.object.clone();
    } else {
        //If we're using a Vector2 object create a Plane, otherwise create a Box
        if (object.geometry && object.geometry instanceof THREE.Geometry) {
            geometry = object.geometry;
        } else if (object.size instanceof THREE.Vector2) {
            var segments = (object.segments) ? object.segments : new THREE.Vector2(1, 1);
            geometry = new THREE.PlaneGeometry(object.size.x, object.size.y, segments.x, segments.y);
        } else {
            var segments = (object.segments) ? object.segments : new THREE.Vector3(1, 1, 1);
            geometry = new THREE.BoxGeometry(object.size.x, object.size.y, object.size.z, segments.x, segments.y, segments.z);
        }

        //If we've defined a specific repeat number, clone the texture
        if (object.texture && object.textureRepeat && object.textureRepeat instanceof THREE.Vector2) {
            texture = object.texture.clone();
            texture.needsUpdate = true; //http://stackoverflow.com/questions/18218733/three-js-cloning-textures-and-onload
            texture.repeat.set(object.textureRepeat.x, object.textureRepeat.y);
        } else if (object.texture) {
            texture = object.texture;
        }

        //Use a custom material function if defined, otherwise just use basic
        var materialFnc = (object.material) ? object.material : "Basic";

        var material = new THREE["Mesh" + materialFnc + "Material"]();
        if (object.color) material.color.setHex(object.color);
        if (texture) material.map = texture;
        if (object.doubleSided) material.side = THREE.DoubleSide;
        if (object.shininess) material.shininess = object.shininess;

        mesh = new THREE.Mesh(geometry, material);
    }

    if (object.position) mesh.position.set(object.position.x, object.position.y, object.position.z);
    if (object.rotation) mesh.rotation.set(object.rotation.x * (Math.PI/180), object.rotation.y * (Math.PI/180), object.rotation.z * (Math.PI/180));

    //Set the scale if specified
    if (object.scale && object.scale instanceof THREE.Vector3) {
        mesh.scale.set(object.scale.x, object.scale.y, object.scale.z);
    }

    //If the mesh is static, disable automatic updates of it's matrix - https://github.com/mrdoob/three.js/wiki/Updates
    if (!object.animated) {
        mesh.matrixAutoUpdate = false;
        mesh.updateMatrix();
    }

	if (object.reference) meshes[object.reference] = mesh;
	if (!removeFromScene) scene.add(mesh);

	return mesh;
}

//Shows debug information in a semi-transparent box
var showDebugInfo = function() {
    var camObj = controls.getObject();

    var debugString = "X Pos: " + camObj.position.x + "<br />";
    debugString += "Y Pos: " + camObj.position.y + "<br />";
    debugString += "Z Pos: " + camObj.position.z + "<br /><br />";
    debugString += "X Rot: " + camObj.children[0].rotation.x * (180/Math.PI) + "<br />";
    debugString += "Y Rot: " + camObj.rotation.y * (180/Math.PI) + "<br />";
    debugString += "Z Rot: " + camObj.rotation.z * (180/Math.PI);

    $("#debug").show();
    $("#debug").html(debugString);
}

//Toggles a top-down view of the scene
var toggleTopDown = function() {
    //Reset position and rotation
    controls.reset();

    if (mouseControlsEnabled) {
        mouseControlsEnabled = false;

        //Set camera to a top-down view
        controls.getObject().position.y = 260;
        controls.getObject().position.z = 0;
        controls.getObject().children[0].rotation.x = 270 * (Math.PI/180);
    } else {
        mouseControlsEnabled = true;
    }
}
