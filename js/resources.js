/*
 * CS32310 Assignment - Kieran Dunbar - kid10
 * resources.js - Handles the loading and storage of world resources (textures, objects etc.)
 */

var textures = {};
var textureLoader = new THREE.TextureLoader();
var numLoadedTextures = 0;
var images = {
    //Structure Textures
    "woodenFloor": new THREE.Vector2(4, 4),
    "carpetFloor": new THREE.Vector2(10, 17),
    "stoneFloor": new THREE.Vector2(2, 20),
    "grassFloor": new THREE.Vector2(2, 2),
    "brickWall": new THREE.Vector2(25, 1),
    "brickWall2": new THREE.Vector2(2, 2),
    "creamWall": new THREE.Vector2(6, 2),
    "slateRoof": new THREE.Vector2(4, 3),
    "metal": new THREE.Vector2(1, 1),
    "burntWood": new THREE.Vector2(1, 1),
    "woodenDoor": new THREE.Vector2(1, 1),

    //Object Textures
    "bed": new THREE.Vector2(1, 1),
    "chair": new THREE.Vector2(1, 1),
    "chair2": new THREE.Vector2(1, 1),
    "painting1": new THREE.Vector2(1, 1),
    "painting2": new THREE.Vector2(1, 1),
    "painting3": new THREE.Vector2(1, 1),
    "painting4": new THREE.Vector2(1, 1),
    "painting5": new THREE.Vector2(1, 1),
    "painting6": new THREE.Vector2(1, 1),
    "wardrobe": new THREE.Vector2(1, 1),
    "woolFloor": new THREE.Vector2(1, 1),

    //skyMap
    "skymap_negx": 0,
    "skymap_negz": 0,
    "skymap_posx": 0,
    "skymap_posy": 0,
    "skymap_posz": 0
};

//For each image defined above, create a texture in the textures object
$.each(images, function(image, repeat) {
    textureLoader.load(
        "assets/textures/" + image + ".jpg",
        function(texture) {
            if (repeat != 0) {
                texture.wrapS = THREE.RepeatWrapping;
                texture.wrapT = THREE.RepeatWrapping;
                texture.repeat.set(repeat.x, repeat.y);
            } else {
                texture.minFilter = THREE.LinearFilter;
            }

            textures[image] = texture;
            numLoadedTextures++;
        }
    );
});

var objectGeometries = {};
var objectLoader = new THREE.JSONLoader();
var numLoadedObjects = 0;
var objectFiles = [
    "fireplace",
    "fireplaceGrill",
    "chair",
    "armchair",
    "drawers",
    "coatstand",
    "bed",
    "table",
    "wardrobe",
    "fridge",
    "counter",
    "counterNoDoors",
    "oven"
];

$.each(objectFiles, function(key, object) {
    objectLoader.load(
        "assets/objects/" + object + ".json",
        function(geometry, materials) {
            //If our object contains materials, add it to our array as a Mesh
            if (materials) {
                var material = new THREE.MultiMaterial(materials);
                objectGeometries[object] = new THREE.Mesh(geometry, material);
            } else {
                objectGeometries[object] = geometry;
            }

            numLoadedObjects++;
        }
    );
});

var resourcesLoaded = function() {
    return numLoadedTextures === Object.keys(images).length && numLoadedObjects === Object.keys(objectFiles).length;
}
